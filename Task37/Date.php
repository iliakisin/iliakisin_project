<?php

namespace Task37;

class Date
{
    public $year;
    public $month;
    public $day;

    private function calculateDate()
    {
        return $this->day;
    }


//Задача 37.3: С помощью магии сделайте свойство weekDay,
// которое будет возвращать день недели, соответствующий дате.

    public function __get($weekDay)
    {
        if($weekDay == 'weekDay')
        return $this->$weekDay = date('D', time());
    }

//Задача 37.2: С помощью магии сделайте так, чтобы в классе могли появляться
// новые свойства, без их прописывания в структуре класса
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    //Задача 37.4: Сделайте возможность вызова 2 методов,
    // которые добавятся в наш класс со временем,
    // но сейчас их нет или они не доступны.
    public function __call($name, $arguments)
    {
        if($name == 'calculateDate' || $name == 'calculateTime')
            return $this->$name($this);
    }

}

// Задача 37.2: С помощью магии сделайте так, чтобы в классе могли появляться
// новые свойства, без их прописывания в структуре класса, засетьте 2 таких
// свойства и выведите их на экран..
$date = new Date();
$date->minute = 25;
echo $date->minute;

echo "<br>";

$date->hours = 3;
echo $date->hours;

$date->calculateDate();
echo $date->calculateDate();