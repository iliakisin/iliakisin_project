<?php

namespace Task12;

class City
{

    public $name;
    public $foundation;
    public $population;
    public $props = array();

    public function addName()
    {
        $this->props[] = $this->name;
    }

    public function addPopulation()
    {
        $this->props[] = $this->population;
    }

    public function addFoundation()
    {
        $this->props[] = $this->foundation;
    }

}


$city = new City();

$city->name = "Kiev";
$city->addName();
$city->foundation = 988;
$city->addFoundation();
$city->population = "3mln";
$city->addPopulation();


foreach ($city->props as $item) {
    echo $item . "<br>";
}

