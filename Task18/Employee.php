<?php

namespace Task18;

require_once "Post.php";

class Employee
{
    public $post;
    public $name;
    public $surname;


    public function __construct(string $name, string $surname, object $object)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $object;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname)
    {
        $this->name = $surname;
    }

    public function changePost(Post $object)
    {
        $this->post = $object;
    }
}

$programmer = new Post("Programmer", 1000);
$manager = new Post("Manager", 2000);
$driver = new Post("Driver", 3000);

$employee1 = new Employee("Ivan", "Krasko", $programmer);

echo $employee1->getName() . PHP_EOL . $employee1->getSurname() . PHP_EOL . $employee1->post->getName() . $employee1->post->getSalary();

$employee1->changePost($manager);
