<?php

namespace Task5;

class Rectangle
{
    public $width;
    public $height;

    public function getSquare($width, $height)
    {
        return $width * $height;
    }

    public function getPerimeter($width, $height)
    {
        return ($width + $height) * 2;
    }
}

$rectangle = new Rectangle();

echo $rectangle->getPerimeter(5, 15);

