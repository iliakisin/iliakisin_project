<?php

namespace Task36;

class User
{
    private $name;
    private $surname;
    private $patronymic;

    public function __construct($name, $surname, $patronymic)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic;
    }

    public function __toString()
    {
        return $this->surname . PHP_EOL . $this->name . PHP_EOL . $this->patronymic . PHP_EOL;
    }

}

