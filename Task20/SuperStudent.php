<?php

namespace Task20;

use Task8\Student;

require_once "../Task8/Student.php";

class SuperStudent extends Student
{
    public function check()
    {
        if (parent::checkName() && parent::checkSurname()) {
            if (is_string($this->patronymic) && strlen($this->patronymic <= 10) &&
                in_array(array('name' => $this->name, 'surname' => $this->surname, 'patronymic' => $this->patronymic), $this->arr)) {
                echo $this->name . PHP_EOL. $this->surname . PHP_EOL. $this->patronymic . PHP_EOL. "успешно сдал экзамен";
            }
        }

    }
}

$student=new SuperStudent();
$student->add("iban","ibanov","ibanovych");


$student->name = "iban";
$student->surname = "ibanov";
$student->patronymic = "ibanovych";

$student->check();
