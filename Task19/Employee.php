<?php

namespace Task19;

use Task3\User;

require_once "../Task3/User.php";

class Employee extends User
{
    public $salary;

    public function doubleCheckAge($age)
    {
        if ($age <= 25) {
            parent::setAge($age);
        }
    }

}