<?php

namespace Task19;

require_once "Employee.php";

class Driver extends Employee
{
    private $driverExperience;
    private $driverLicense;

    public function getDriverExperience()
    {
        return $this->driverExperience;
    }

    public function setDriverExperience($driverExperience)
    {
        $this->driverExperience = $driverExperience;

    }

    public function getDriverLicense()
    {
        return $this->driverLicense;
    }

    public function setDriverLicense($driverLicense)
    {
        $this->driverLicense = $driverLicense;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    //Задача 19.5: Добавьте в класс User 1 свойство и 2 метода для него,
    // которые нельзя будет использовать снаружи класса, но можно будет
    // использовать в классе  Driver. Используйте это свойство и методы.
    public function getSurname()
    {
        return $this->surname;
    }

    public function __construct($name, $age, $driverExperience, $driverLicense)
    {
        parent::__construct($name, $age);
        $this->driverExperience = $driverExperience;
        $this->driverLicense = $driverLicense;
    }

    public function setAge($age)
    {
        if ($age <= 40) {
            parent::setAge($age);
            }
    }


}

$driver = new Driver("shawn", 23, 5, "A");
$driver->setSurname("alekseenko");
