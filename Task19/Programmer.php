<?php

namespace Task19;

require_once "Employee.php";

class Programmer extends Employee
{
    private $langs = [];

    public function getLang()
    {
        return $this->langs;
    }

    public function setLang($lang)
    {
        $this->langs[] = $lang;
    }
}