<?php

namespace Task2;

class Employee
{
    public $name;
    public $age;
    public $salary;

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function checkAge()
    {
        return $this->age <= 18 ? FALSE : TRUE;
    }

}

$employee1 = new Employee();
$employee1->name = "Иван";
$employee1->age = 25;
$employee1->salary = 1000;

$employee2 = new Employee();
$employee2->name = "Вася";
$employee2->age = 26;
$employee2->salary = 2000;

// Найдем сумму зарплат:
echo $employee1->salary + $employee2->salary . "<br>";

// Найдем сумму возрастов:
echo $employee1->age + $employee2->age . "<br>";

echo $employee1->getSalary() + $employee2->getSalary() . "<br>";



