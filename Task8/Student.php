<?php

namespace Task8;

class Student
{
    public $name;
    public $surname;
    public $patronymic;

    public $arr = [];
    public $course;

    private $courseAdministrator;

    public function transferToNextCourse()
    {
        if ($this->isCourseCorrect())
            echo $this->course = $this->course + 1;
    }

    private function isCourseCorrect(): bool
    {
        return $this->course < 5 & $this->course > 0;

    }

    public function getCourseAdministrator()
    {
        return $this->courseAdministrator;
    }

    public function setCourseAdministrator($courseAdministrator)
    {
        $this->courseAdministrator = $courseAdministrator;
    }

    //Задача 20.2: Добавьте в этот класс валидатор для проверки имени и
    // фамилии курс-координатора, сделайте так, чтобы его можно было
    // использовать в классах наследниках.

    protected function checkName():bool
    {
        return is_string($this->name) && strlen($this->name <= 10);

    }
    protected function checkSurname():bool
    {
        return is_string($this->surname) && strlen($this->surname <= 10);

    }

    public function add($name, $surname, $patronymic)
    {
        $this->arr[] = [
            'name' => $name,
            'surname' => $surname,
            'patronymic' => $patronymic,
        ];
    }


}


