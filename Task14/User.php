<?php

namespace Task14;

class User
{
    private $surname;
    private $name;
    private $patronymic;

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    public function getFullName()
    {
        return $this->surname[0] . $this->name[0] . $this->patronymic[0];
    }

}

$user = new User();
echo $user->setName("Ilia")->setSurname("Kisin")->setPatronymic("Konstantynovich")->getFullName();

/*
 $user->setName("Ilia");
$user->setSurname("Kisin");
$user->setPatronymic("Konstantynovich");
$user->getFullName();
echo $user->getFullName();
*/

