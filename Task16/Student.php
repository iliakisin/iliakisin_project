<?php

namespace Task16;

require_once "User.php";

class Student
{
    public $name;
    public $scholarship;

    public function __construct($name, $scholarship)
    {
        $this->name = $name;
        $this->scholarship = $scholarship;
    }


}
