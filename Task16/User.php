<?php

namespace Task16;

require_once "Employee.php";
require_once "Student.php";

echo "Task16 <br><br>";

class User
{
    public $arr = [];

    public function addStudent(object $object)
    {
        $this->arr[] = [
            'student' => [
                'name' => $object->name,
                'scholarship' => $object->scholarship
            ]
        ];
    }

    public function addEmployee(object $object)
    {
        $this->arr[] = [
            'employee' => [
                'name' => $object->name,
                'salary' => $object->salary
            ]
        ];
    }

}

$employee1 = new Employee("Worker1", 1000);
$employee2 = new Employee("Worker2", 1500);
$employee3 = new Employee("Worker3", 2000);

$student1 = new Student("Student1", 1100);
$student2 = new Student("Student2", 1200);
$student3 = new Student("Student3", 1300);

$user = new User();

$user->addEmployee($employee1);
$user->addStudent($student1);

$user->addEmployee($employee2);
$user->addStudent($student2);

$user->addEmployee($employee3);
$user->addStudent($student3);

echo "
<table>
<tr>
<th> Employees </th>
<th> Students </th>
</tr>";

foreach ($user->arr as $number => $category) {
    foreach ($category as $person => $list) {
        foreach ($list as $name => $value) {
            if ($person == "employee" & $name == "name") {
                echo "<tr><td>" . $value;
            } elseif ($person == "student" & $name == "name") {
                echo "<td>" . $value . "</td></tr>";
            }
        }
    }
}
echo "</table>";

echo "
<table>
<tr>
<th> Employees total salary </th>
<th> Students total scholarship </th>
</tr>";

$employeesSalary = 0;
$studentsScholarship = 0;

foreach ($user->arr as $number => $category) {
    foreach ($category as $person => $list) {
        foreach ($list as $name => $value) {
            if ($person == "employee" & $name == "salary") {
                $employeesSalary += $value;

            } elseif ($person == "student" & $name == "scholarship") {
                $studentsScholarship += $value;
            }
        }
    }
}
echo "<tr><td>" . $employeesSalary;
echo "<td>" . $studentsScholarship . "</td></tr>";

echo "</table>";
