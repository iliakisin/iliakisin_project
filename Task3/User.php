<?php
namespace Task3;
class User
{
    public $name;
    public $age;

    //Задача 19.5: Добавьте в класс User 1 свойство и 2 метода для него,
    // которые нельзя будет использовать снаружи класса, но можно будет
    // использовать в классе  Driver. Используйте это свойство и методы.
    protected $surname;
    protected function __construct($name,$age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    protected function setAge($age)
    {
        if ($this->isValidAge($age)) {
            $this->age = $age;
        }

    }

    public function isValidAge($years): bool
    {
        return is_int($years) && $years >= 18;

    }
}




