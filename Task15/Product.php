<?php

namespace Task15;

//Задача 15.1: Сделайте класс Product (товар), в котором будут приватные свойства
// name (название товара), price (цена за штуку) и quantity.
// Пусть все эти свойства будут доступны только для чтения.
class Product
{
    private $name;
    private $price;
    private $quantity;

    public function __construct($name, $price, $quantity)
    {
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

//Задача 15.2: Добавьте в класс Product метод getCost,
// который будет находить полную стоимость продукта (сумма умножить на количество).
    public function getCost()
    {
        return ($this->price * $this->quantity);
    }
}



