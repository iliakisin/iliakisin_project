<?php

namespace Task15;

require_once '../Task15/Product.php';
//Задача 15.3: Сделайте класс Cart (корзина).
// Данный класс будет хранить список продуктов (объектов класса Product) в виде массива.
// Пусть продукты хранятся в свойстве products.
class Cart
{
    public $products = [];

//Задача 15.4: Реализуйте в классе Cart метод add для добавления продуктов.
    public function add(Product $product)
    {
        $this->products[] = [
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'quantity' => $product->getQuantity(),
            'cost' => $product->getCost()
        ];
    }

//Задача 15.5: Реализуйте в классе Cart метод remove для удаления продуктов.
// Метод должен принимать параметром название удаляемого продукта.
    public function remove(Product $prod)
    {
        foreach ($this->products as $key => $value) {
            foreach ($value as $k => $v) {
                if ($v == $prod->getName()) {
                    unset($this->products[$key]);
                }
            }
        }
    }

//Задача 15.6: Реализуйте в классе Cart метод getTotalCost, который будет находить суммарную стоимость продуктов.
    public function getTotalCost()
    {
        $totalCost = 0;
        foreach ($this->products as $key => $value) {
            foreach ($value as $k => $v) {
                if ($k == 'cost') {
                    $totalCost += $v;
                }
            }
        }
        return $totalCost;
    }

//Задача 15.7: Реализуйте в классе Cart метод getTotalQuantity,
// который будет находить суммарное количество продуктов (то есть сумму свойств quantity всех продуктов).
    public function getTotalQuantity()
    {
        $totalQuantity = 0;
        foreach ($this->products as $key => $value) {
            foreach ($value as $k => $v) {
                if ($k == 'quantity') {
                    $totalQuantity += $v;
                }
            }
        }
        return $totalQuantity;
    }

//Задача 15.8: Реализуйте в классе Cart метод getAvgPrice,
// который будет находить среднюю стоимость продуктов (суммарная стоимость делить на количество всех продуктов).
    public function getAvgPrice()
    {
        $avgPrice = ($this->getTotalCost() / $this->getTotalQuantity());
        return round($avgPrice, 2);
    }

}

$product1 = new Product("rol", 35, 2);
$product1->getName();
$product1->getPrice();

$product2 = new Product("rolla", 38, 5);
$product2->getName();
$product2->getPrice();


$cart = new Cart();
$cart->add($product1);
//$cart1 = new Cart();
//$cart1->add($object2);
$cart->add($product2);

//var_dump($cart->products);

$cart->getTotalCost();
echo $cart->getTotalCost();
//var_dump($cart->products);


