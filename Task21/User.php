<?php

namespace Task21;

class User
{
    private $name;
    private $surname;
    private $birthday;
    private $age;

    public function __construct($name, $surname, $birthday)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
    }


    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getAge()
    {
        return $this->age;
    }

    protected function calculateAge($birthday)
    {
        $birthday = explode("-", $birthday);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthday[1], $birthday[2], $birthday[0]))) > date("md")
            ? ((date("Y") - $birthday[0]) - 1)
            : (date("Y") - $birthday[0]));
        return $age;

    }
}

//$obj = new User("iban", "nom","1995-08-02");
//echo $obj->getAge();
