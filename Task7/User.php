<?php

namespace Task7;

class User
{
    public $name;
    public $age;

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function addAge($ages_added)
    {
        if ($this->isValidAge($ages_added))
            return $this->age = $this->age + $ages_added;

    }

    public function subAge($ages_decreased)
    {
        if ($this->isValidAge($ages_decreased))
            return $this->age = $this->age - $ages_decreased;

    }

    private function isValidAge($years): bool
    {
        return is_int($years) & $years >= 5;

    }
}

$user = new User();
$user->name = "Коля";
$user->age = 25;

/*$user->setAge(30);

$user->addAge(5);
echo $user->age;
echo "<br>";
$user->subAge(20);
echo $user->age;
*/
echo $user->isValidAge(6);


