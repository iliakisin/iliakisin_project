<?php

namespace Task17;

require_once "Employee.php";
require_once "User.php";
require_once "City.php";

class Arri
{
    public $arr = [];

    public function add(object $object)
    {
        $this->arr[] = $object;
    }


}


$employee1 = new Employee("Employee1", "Rise", 350);
$employee2 = new Employee("Employee2", "Bryant", 500);
$employee3 = new Employee("Employee3", "Wade", 750);

$user1 = new User("User1", "Paul");
$user2 = new User("User2", "James");
$user3 = new User("User3", "Westbrook");

$city1 = new City("Kiev", "4mln");
$city2 = new City("SPB", "5mln");
$city3 = new City("London", "3mln");


$array = new Arri();
$array->add($employee1);
$array->add($employee2);
$array->add($employee3);

$array->add($user1);
$array->add($user2);
$array->add($user3);

$array->add($city1);
$array->add($city2);
$array->add($city3);

//Задача 17.5: Переберите циклом массив $arr и выведите на экран столбец
// свойств name тех объектов, которые принадлежат классу User или потомку этого класса.

foreach ($array->arr as $key => $object) {
    if ($object instanceof User) {
        echo $object->name . "<br>";
    }

}

//Задача 17.6: Переберите циклом массив $arr и выведите на экран столбец
// свойств name тех объектов, которые НЕ принадлежат классу User или потомку этого класса.

foreach ($array->arr as $key => $object) {
    if (!($object instanceof User)) {
        echo $object->name . "<br>";
    }

}

//Задача 17.7: Переберите циклом массив $arr и выведите на экран столбец свойств
// name тех объектов, которые принадлежат именно классу User, то есть не классу City и не классу Employee.
foreach ($array->arr as $key => $object) {
    if ($object instanceof User & !($object instanceof Employee)) {
        echo $object->name . "<br>";
    }

}
