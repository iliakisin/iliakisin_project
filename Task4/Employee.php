<?php

namespace Task4;

class Employee
{
    public $name;
    public $salary;

    public function doubleSalary()
    {
        return 2 * ($this->salary);
    }
}

$employee = new Employee();
$employee->salary = 1000;
echo $employee->doubleSalary();


