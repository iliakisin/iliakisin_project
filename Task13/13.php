<?php

namespace Task13;

use Task2\Employee;

require_once '../Task2/Employee.php';

$methods = ['method1' => 'getName', 'method2' => 'getAge'];

$employee = new Employee();

$employee->age = 13;
$employee->name = "Iban";


//First way to resolve Task13
echo $employee->{$methods['method1']}() . PHP_EOL;
echo $employee->{$methods['method2']}() . PHP_EOL;

echo "<br>";

//Second way to resolve Task13
echo call_user_func([$employee, $methods['method1']]) . PHP_EOL;
echo call_user_func([$employee, $methods['method2']]) . PHP_EOL;


