<?php

namespace Task23;

class Num
{
    public static $num1;
    public static $num2;
    public static $num3 = 3;
    public static $num4 = 4;

    const NUMBER_P = 3.14;

    const INFINITY = 'бесконечность';

    public static function getSum()
    {
        return self::$num1 + self::$num3;

    }

    public function getConstant()
    {
        return self::INFINITY;
    }

}

Num::$num1 = 2;
Num::$num2 = 3;

echo Num::$num1 + Num::$num2;
echo "<br>";
echo Num::NUMBER_P;
echo "<br>";
$number = new Num();
echo $number->getConstant();