<?php

namespace Task9;

class Employee
{
    private $name;
    private $age;
    private $salary;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        if ($this->isAgeCorrect($age))
            return $this->age = $age;
    }

    public function getSalary()
    {
        return $this->salary . "$";
    }


    public function setSalary($salary)
    {
        $this->salary = $salary;
    }


    private function isAgeCorrect($years): bool
    {
        return is_int($years) & $years >= 1 & $years <= 100;
    }

}

$employee = new Employee();

$employee->setAge(100);
$employee->getAge();
echo $employee->getAge();
echo "<br>";
$employee->setSalary(100);
$employee->getSalary();
echo $employee->getSalary();

