<?php

namespace Task22;

class ArraySumHelper
{

    public static function calculate($arr, $multiply)
    {
        $array = [];

        foreach ($arr as $key) {

            $array[] = $key * $multiply;
        }
        return $array;
    }
}

//echo ArraySumHelper::calculate([2, 4, 6, 8], 5);

/*$arra = new ArraySumHelper();
var_dump($arra->calculate([2, 4, 6, 8], 5));

